package com.example.android.quakereport;

public class Earthquake {

    private String mLocation, mUrl;
    private long mUnixTime;
    private double mMagnitude;

    public Earthquake(double mag, String loca, long date, String url){
        this.mMagnitude = mag;
        this.mLocation = loca;
        this.mUnixTime = date;
        this.mUrl = url;
    }

    public double getMagnitude(){
        return mMagnitude;
    }

    public String getLocation(){
        return mLocation;
    }

    public long getUnixTime(){
        return mUnixTime;
    }

    public String getUrl() { return mUrl; }

}
